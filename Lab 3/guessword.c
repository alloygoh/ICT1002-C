#include <stdio.h>
#include <ctype.h>
#include <string.h>

int charCheck(char x){
    if (!isalpha(x)){
        return -1;
    }
    return tolower(x);
}

int main(){
    int flag = 1;
    char p1Input[13];

    while (flag) {
        printf("Player 1, enter a word of no more than 12 letters:\n");

        int read = strlen(fgets(p1Input,13,stdin));
        // clear stdin to prevent input from overflow next prompt
        if (p1Input[read-1]!= '\n'){
            while(getchar() != '\n' ) /* do nothing*/ ;
        }
        p1Input[strcspn(p1Input, "\n")] = '\0';

        for (int i = 0; i < strlen(p1Input); i++) {
            int result = charCheck(p1Input[i]);
            if (result >= 0) {
                p1Input[i] = (char) result;
                flag = 0;
                continue;
            }
            printf("\n");
            printf("Sorry, the word must contain only English letters.\n");
            flag = 1;
            break;
        }
    }
    int count = 7;
    char guess[strlen(p1Input)];
    memset(guess,'_',strlen(p1Input));
    guess[strlen(p1Input)] = '\0';
    while(count >0){
        printf("Player 2 has so far guessed:");
        for (int i = 0; i < strlen(guess);i++){
            printf(" ");
            printf("%c",guess[i]);
        }
        if (strchr(guess,'_') == NULL){
            printf("\nPlayer 2 wins.");
            return 0;
        }
        printf("\nPlayer 2, you have %d guesses remaining. Enter your next guess:\n",count);
        char p2Input;
        scanf("%c",&p2Input);
        while( p2Input != '\n' && getchar() != '\n' ) /* do nothing*/ ;
        int present = 0;
        for (int i=0;i<strlen(p1Input);i++){
            if (p1Input[i] == p2Input){
                guess[i] = p2Input;
                present = 1;
            }
        }
        if (!present){
            count--;
        }
    }
    return 0;
}
