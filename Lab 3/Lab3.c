#include <stdio.h>
#include <string.h>

typedef struct {
    float moneyValue;
    char currency[4];
} INTL_MONEY_VALUE;
typedef INTL_MONEY_VALUE* INTL_MONEY_VALUE_PTR;

int ex1(){
    int *zPtr;
    int *aPtr = NULL;
    void *sPtr = NULL;
    int number, i;
    int z[5] = {1,2,3,4,5};
    zPtr = z;
    sPtr = z;

    number = *zPtr;
    printf("a) %d\n",number);
    // shift pointer to index 3
    number = *(zPtr+2);
    printf("b) %d\n",number);
    // <= results in off-by-one
    for (int i = 0; i < 5; i++) {
        printf("%d",zPtr[i]);
    }
    return 0;
}

int ex2(){
    long value1 = 200000;
    long value2 = 300000;
    // a
    long *lPtr;
    // b
    lPtr = &value1;
    // c
    printf("%d\n",*lPtr);
    // d
    *lPtr = value2;
    // e
    printf("%d\n",value2);
    // f
    printf("%p\n",&value1);
    // g
    printf("%p\n",lPtr);
    return 0;
}

int ex3(){
    INTL_MONEY_VALUE test;
    strcpy(test.currency,"USD");
    test.moneyValue = 1.1;
    INTL_MONEY_VALUE_PTR testPtr = &test;
    printf("%f%s\n",test.moneyValue,test.currency);
    printf("%p",testPtr);
}

int main(){
    //ex1();
    //ex2();
    ex3();
}