#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*
 * Standard Archive Format -
 Standard TAR –
 USTAR
 * from https://www.fileformat.info/format/tar/corion.htm
 */
#define  RECORDSIZE  512
#define  NAMSIZ      100
#define  TUNMLEN      32
#define  TGNMLEN      32
struct header
{
    char    name[NAMSIZ];
    char    mode[8];
    char    uid[8];
    char    gid[8];
    char    size[12];
    char    mtime[12];
    char    chksum[8];
    char    linkflag;
    char    linkname[NAMSIZ];
    char    magic[8];
    char    uname[TUNMLEN];
    char    gname[TGNMLEN];
    char    devmajor[8];
    char    devminor[8];
};

int main(int argc, char **argv){
    if (argc != 3){
        printf("Invalid argument length.");
        return -1;
    }

    FILE *target = fopen("out.tar","wb");
    if (target == NULL){
        printf("Failed to create output file");
        return -1;
    }

    for (int i = 1; i < argc;i++ ){
        FILE *f = fopen(argv[i],"rb");
        if (f == NULL){
            printf("Could not open target files.");
            return -1;
        }

        // get file size
        fseek(f,0,SEEK_END);
        int fSize = ftell(f);
        rewind(f);

        struct header h1;
        strncpy(h1.name, argv[i],sizeof h1.name);
        sprintf(h1.size, "%d",fSize);

        // allocate memory to hold file data
        void *fData = calloc(fSize,sizeof(char));
        if (fData == NULL){
            printf("Failed to allocate data");
            return -1;
        }
        int result = fread(fData,1,fSize,f);
        if (result != fSize){
            printf("Failed to read from file");
            return -1;
        }
        fclose(f);
        int bytesWritten = fwrite(&h1,1, sizeof(h1),target);
        if (bytesWritten != sizeof h1){
            printf("Failed to write header to output file");
            return -1;
        }
        bytesWritten = fwrite(fData,1,fSize,target);
        if (bytesWritten != fSize){
            printf("Failed to write data to output file");
            return -1;
        }
        free(fData);
    }
    fclose(target);
}