#include <string.h>
#include <stdio.h>

int main(){
    struct grade_node {
        char surname[20];
        double grade;
        struct grade_node *next;
    };
    typedef struct grade_node GRADE_NODE;
    typedef GRADE_NODE *GRADE_NODE_PTR;

    // 1
    GRADE_NODE_PTR head;
    // 2
    GRADE_NODE tmp;
    strcpy(tmp.surname,"Adams");
    tmp.grade = 85.0;
    head = &tmp;
    // 3
    GRADE_NODE t2;
    strcpy(t2.surname,"Pritchard");
    t2.grade = 66.5;
    head->next = &t2;
    // 4
    GRADE_NODE t3;
    strcpy(t3.surname,"Jones");
    t3.grade = 91.5;
    GRADE_NODE_PTR temp = head->next;
    t3.next = temp;
    head->next = &t3;
    temp->next = NULL;
    GRADE_NODE_PTR current = head;
    while(current != NULL){
        printf("%s\n",current->surname);
        current = current->next;
    }
    return 0;
}