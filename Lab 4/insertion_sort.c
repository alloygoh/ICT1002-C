#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

int prompt(char *buf){
    printf("Please enter a word:");
    fgets(buf,33,stdin);
    if (strncmp(buf,"***",3) == 0){
        return 1;
    }
    for (int i=0;i<strlen(buf);i++){
        if (!(isalpha(buf[i]) || buf[i] == '\'' || buf[i] != '-')){
            return -1;
        }
        buf[i] = tolower(buf[i]);
    }
    char *nl = strchr(buf,'\n');
    if (nl != NULL){
        *nl = '\0';
        return 0;
    }
    int ch;
    while ((ch=getchar()) != EOF && ch != '\n'); /* flush stdin to prevent input overflow */
    return 0;
}
int main(){
    typedef struct node{
        char word[33];
        struct node *next;
    } node;
    typedef node *node_ptr;
    node_ptr head = NULL;
    while (1){
        // to store input
        char inputBuf[33];
        int response = prompt(inputBuf);
        if (response){
            break;
        }
        node_ptr tmp = (node_ptr)calloc(1,sizeof(node));
        if (tmp == NULL){
            printf("Memory allocation failed");
            return -1;
        }
        // add values into node
        strcpy(tmp->word,inputBuf);
        tmp->next = NULL;

        if (head == NULL){
            head = tmp;
            continue;
        }
        node_ptr current = head;
        int insert = 0;
        while (current->next != NULL){
            if (strcmp(current->word,tmp->word) < 0 && strcmp(current->next->word,tmp->word) >= 0){
                // insert between
                node_ptr hold = current->next;
                current->next = tmp;
                tmp->next = hold;
                insert = 1;
                break;
            }
            current = current->next;
        }
        // add to tail
        if (!insert){
            current->next = tmp;
        }
    }
    // dump list and free list
    node_ptr current = head;
    while (current != NULL){
        node_ptr next = current->next;
        printf("%s\n",current->word);
        free(current);
        current = next;
    }
    return 0;
}
