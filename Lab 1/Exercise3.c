#include <stdio.h>
int main(){
    float height, weight;
    printf("Enter your weight in kilograms: ");
    scanf("%f",&weight);
    printf("Enter your height in metres: ");
    scanf("%f",&height);
    float bmi = weight / (height * height);
    if (bmi < 18.5){
        printf("Your BMI is %.1f. That is within the underweight range.",bmi);
    }
    else if (bmi <= 24.9){
        printf("Your BMI is %.1f. That is within the normal range.",bmi);
    }
    else if (bmi <= 29.9){
        printf("Your BMI is %.1f. That is within the overweight range.",bmi);
    }
    else{
        printf("Your BMI is %.1f. That is within the obese range.",bmi);
    }
    return 0;
}
