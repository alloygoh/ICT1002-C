#include <stdio.h>

int main(){
    int count = 10;
    int input;
    printf("Player 1, enter a number between 1 and 1000:\n");
    scanf("%d",&input);
    int isvalid = (1 <= input && input <= 1000);
    while (!isvalid){
        printf("That number is out of range.\n");
        printf("Player 1, enter a number between 1 and 1000:\n");
        scanf("%d",&input);
        isvalid = (1 <= input && input <= 1000);
    }
    // valid input
    int p2input;
    for (int count = 10; count > 0;count--){
        printf("Player 2, You have %d guesses remaining.\nEnter Your Guess:\n",count);
        scanf("%d",&p2input);
        if (p2input < input){
            printf("Too Low.\n");
        }
        else if(p2input > input){
            printf("Too High.\n");
        }
        else{
            printf("Player 2 Wins.\n");
            return 0;
        }
    }
    printf("Player 1 Wins.");
    return 0;
}