#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main() {
    char input[255];
    char pattern[255];
    char caseSensitive;
    printf("Enter text:\n");
    fgets(input, 255, stdin);
    input[strcspn(input, "\n")] = '\0';

    printf("Enter pattern:\n");
    fgets(pattern, 255, stdin);

    printf("Case sensitive?\n");
    scanf("%c", &caseSensitive);

    if (tolower(caseSensitive) == 'y') {
        for (int i = 0; i < strlen(input); i++) {
            int matched = 0;
            for (int j = 0; j < strlen(pattern) && pattern[j] != '\n'; j++) {
                if (pattern[j] == '_' && isspace(input[i+j])){
                    matched = 1;
                    continue;
                }
                if (input[i + j] == pattern[j] || pattern[j] == '.') {
                    matched = 1;
                    continue;
                }
                matched = 0;
                break;
            }
            if (matched) {
                printf("Found at %d", i);
                return 1;
            }
        }
        printf("Not Found");
        return 0;
    }
    // case insensitive
    for (int i = 0; i < strlen(input); i++) {
        int matched = 0;
        for (int j = 0; j < strlen(pattern) && pattern[j] != '\n'; j++) {
            if (pattern[j] == '_' && isspace(input[i+j])){
                matched = 1;
                continue;
            }
            if (tolower(input[i + j]) == tolower(pattern[j]) || pattern[j] == '.') {
                matched = 1;
                continue;
            }
            matched = 0;
            break;
        }
        if (matched) {
            printf("Found at %d", i);
            return 1;
        }
    }
    printf("Not Found");
    return 0;
}