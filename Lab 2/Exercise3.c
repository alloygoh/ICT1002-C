#include <stdio.h>
#include <string.h>
int main(){
    printf("Enter a sentence, up to 255 characters:\n");
    char input[255];
    //input[255] = '\0';
    fgets(input, 255, stdin);
    // remove newline
    input[strcspn(input,"\n")] = '\0';
    char *input_split = strtok(input, " ,.");
    while(input_split != NULL){
        printf("%s %d\n", input_split, strlen(input_split));
        // get next token from current buffer
        input_split = strtok(NULL, " ");
    }
    return 0;
}